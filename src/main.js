import express from 'express'
import bodyParser from 'body-parser'
import axios from 'axios'
import cors from 'cors'
import { isArray, every, isString, isEmpty, isNumber } from 'lodash'

const axiosInstance = axios.create({
    baseURL: 'http://' + (process.env.API || '127.0.0.1:31567'),
    timeout: 10000
});

const app = express();
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
const allowedOrigins = ['https://zp.io'];
app.use(cors({
    origin: function(origin, callback){
        // allow requests with no origin
        // (like mobile apps or curl requests)
        if(!origin) return callback(null, true);
        if(allowedOrigins.indexOf(origin) === -1){
            const msg = 'The CORS policy for this site does not ' +
                'allow access from the specified Origin.';
            return callback(new Error(msg), false);
        }
        return callback(null, true);
    }
}));

const catchError = function (error,response) {
    if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log(error);
        response.status(error.response.status).send(error.response.data);
        return error.response;
    } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.log(error);
        return error.response;
    } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message);
        return error.message
    }
};

const get = function (url, response) {
    axiosInstance
        .get(url)
        .then(function (res) {
            response.json(res.data)
        })
        .catch(function (error) {
            catchError(error, response)
        })
};

const post = function (url, postData, response) {
    axiosInstance
        .post(url, postData)
        .then(function (res) {
            response.json(res.data)
        })
        .catch(function (error) {
            catchError(error, response)
        })
};

const error = function (message, response) {
    response.status(400).send(message)
};

/**
 *
 * record:
    {
        "addresses": ["abcd1234", "abcd1234" ]
    }
 *
 * response {Address list} request.body   a list of addressess.
 * @return {AddressUTXO}.
 */

app.post('/api/unspentoutputs', (request, response) => {
    const addresses = request.body;

    if (isArray(addresses) && every(addresses, isString)) {
        post('/addressdb/outputs', {
            'addresses': addresses,
            'mode': 'unspentOnly'
        }, response)
    } else {
        error('array of addresses is expected', response)
    }
});

/**
 *
 *  record:
    {
        "addresses": ["abcd1234", "abcd1234" ],
        "skip": 1000,
        "take": 1000
    }
 *
 * body {addresses, skip, take} request   a list of addressees with pagination attributes.
 * @return UTXO of each address in pagination.
 */

app.post('/api/history', (request,response) => {
    const {body} = request;

    if (isEmpty(body.addresses) || !isArray(body.addresses) || !every(body.addresses, isString)) {
        error('addresses is missing or invalid array', response);
        return
    }

    if (!isNumber(body.skip)) {
        error('skip is missing', response);
        return
    }

    if (!isNumber(body.take)) {
        error('take is missing', response);
        return
    }

    post('/addressdb/transactions', {
        'addresses': body.addresses,
        'skip': body.skip,
        'take': body.take
    }, response)
});
/**
 *
 *  record { tx: "string" }
 *
 * body {tx} request  a hex encoded tx.
 * @return post the tx in the node.
 */

app.post('/api/publishtransaction', (request,response) => {
    const {tx} = request.body;

    if (isEmpty(tx) || !isString(tx)) {
        error('expecting a tx field with base16 string', response);
        return
    }

    post('/blockchain/publishtransaction', {tx}, response)
});

/**
 *
 * record:  {
    "address": "address",
    "command": "command",
    "messageBody": "message body",
    "options": {
        "sender": "publickey"
    },
    "tx": "abcdef012346789"
    }
 *
 * body {record} request   a list of addressees with pagination attributes.
 * @return Execution transaction.
 */

app.post('/api/runcontract', (request, response) => {
    const {body} = request;

    if (isEmpty(body.address) || !isString(body.address)) {
        error('address is missing', response);
        return
    }

    if (isEmpty(body.tx) || !isString(body.tx)) {
        error('tx is missing', response);
        return
    }

    let sender = '';

    if (!isEmpty(body.options)) {
        if (!isEmpty(body.options.sender)) {
            sender = body.options.sender
        }
    }

    post('/blockchain/contract/execute', {
        'address': body.address,
        'command': body.command,
        'messageBody': body.messageBody,
        'options': {
            'sender': sender
        },
        'tx': body.tx
    }, response)
});

/**
 *
 * record:  {
    "contractId": "abcd1234",
    "skip": 1000,
    "take": 1000
}
 *
 * body {contractId, skip, take} request   contractId witnesses with pagination attributes.
 * @return wintess of given contract ID with pagination
 */


app.post('/addressdb/contract/history', (request,response) => {
    const {body} = request;

    if (isEmpty(body.contractId) || !every(body.contractId, isString)) {
        error('contractId is missing', response);
        return
    }

    if (!isNumber(body.skip)) {
        error('skip is missing', response);
        return
    }

    if (!isNumber(body.take)) {
        error('take is missing', response);
        return
    }

    post('/addressdb/contract/history', {
        'contractId': body.contractId,
        'skip': body.skip,
        'take': body.take
    }, response)
});

/**
 *
 * record:
    {
        "addresses": ["abcd1234", "abcd1234" ]
    }
 *
 * response {Address list} request.body   a list of addressess.
 * @return transaction count per address.
 */

app.post('/addressdb/transactioncount',(request,response) => {
    const {body} = request;

    if (isEmpty(body.addresses) || !isArray(body.addresses) || !every(body.addresses, isString)) {
        error('addresses is missing or invalid array', response);
        return
    }

    post('/addressdb/transactioncount', {
        'addresses': body.addresses,
    }, response)
});

/**
 * @return Record of active contracts
 */


app.get('/api/activecontracts', (request,response) => {
    get('/contract/active', response)
});

/**
 * @return Record of blockchain information
 */


app.get('/api/info', (request,response) => {
    get('/blockchain/info', response)
});

/**
 * @return Node connection count
 */


app.get('/network/connections/count', (request,response) => {
    get('/network/connections/count', response)
});

app.listen(3000);
